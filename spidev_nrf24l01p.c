/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

static void pabort(const char *s) {
	perror(s);
	abort();
}

static struct spi_config_t spi_config {
	char *device = "/dev/spidev1.1";
	uint8_t mode = 0;
	uint8_t bits = 8;
	uint32_t speed = 500000;
	uint16_t delay;
} = {
	/* default values */
	/* char *device =   */ "/dev/spidev1.1", 
	/* uint8_t mode =   */ 0,
	/* uint8_t bits =   */ 8,
	/* uint32_t speed = */ 500000,
	/* uint16_t delay = */ 1
}

static void print_usage(const char *prog) {
	printf("Usage: %s [-DsbdlHOLC3]\n", prog);
	puts("  -D --device   device to use (default /dev/spidev1.1)\n"
	     "  -s --speed    max speed (Hz)\n"
	     "  -d --delay    delay (usec)\n"
	     "  -b --bpw      bits per word \n"
	     "  -l --loop     loopback\n"
	     "  -H --cpha     clock phase\n"
	     "  -O --cpol     clock polarity\n"
	     "  -L --lsb      least significant bit first\n"
	     "  -C --cs-high  chip select active high\n"
	     "  -3 --3wire    SI/SO signals shared\n");
	exit(1);
}

static void parse_opts(int argc, char *argv[]) {
	while (1) {
		static const struct option lopts[] = {
			{ "device",  1, 0, 'D' },
			{ "speed",   1, 0, 's' },
			{ "delay",   1, 0, 'd' },
			{ "bpw",     1, 0, 'b' },
			{ "loop",    0, 0, 'l' },
			{ "cpha",    0, 0, 'H' },
			{ "cpol",    0, 0, 'O' },
			{ "lsb",     0, 0, 'L' },
			{ "cs-high", 0, 0, 'C' },
			{ "3wire",   0, 0, '3' },
			{ "no-cs",   0, 0, 'N' },
			{ "ready",   0, 0, 'R' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "D:s:d:b:lHOLC3NR", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'D':
			spi_config.device = optarg;
			break;
		case 's':
			spi_config.speed = atoi(optarg);
			break;
		case 'd':
			spi_config.delay = atoi(optarg);
			break;
		case 'b':
			spi_config.bits = atoi(optarg);
			break;
		case 'l':
			spi_config.mode |= SPI_LOOP;
			break;
		case 'H':
			spi_config.mode |= SPI_CPHA;
			break;
		case 'O':
			spi_config.mode |= SPI_CPOL;
			break;
		case 'L':
			spi_config.mode |= SPI_LSB_FIRST;
			break;
		case 'C':
			spi_config.mode |= SPI_CS_HIGH;
			break;
		case '3':
			spi_config.mode |= SPI_3WIRE;
			break;
		case 'N':
			spi_config.mode |= SPI_NO_CS;
			break;
		case 'R':
			spi_config.mode |= SPI_READY;
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}

int spi_transmit(int fd, uint8_t* buffer_tx, uint8_t* buffer_rx, uint8_t len) {
	int ret = 0;

	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)buffer_tx,
		.rx_buf = (unsigned long)buffer_rx,
		.len = len,
		.delay_usecs = spi_config.delay,
		.speed_hz = spi_config.speed,
		.bits_per_word = spi_config.bits,
	};

	ret = ioctl(fd, SPI_IOC_MESSAGE(1), &tr);
	
	return ret;
}

int spi_setup(int fd) {
	int ret = 0;

	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &spi_config.mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &spi_config.mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &spi_config.bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &spi_config.bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_config.speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &spi_config.speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	printf("spi mode: %d\n", spi_config.mode);
	printf("bits per word: %d\n", spi_config.bits);
	printf("max speed: %d Hz (%d KHz)\n", spi_config.speed, spi_config.speed/1000);

	return ret;
}

int radio_init(int fd) {
	int ret = 0;
	
	return ret;
}

/* Read radio STATUS register
   An NOP command is sent over SPI to radio which 
   will send back 8 bits of STATUS register */
uint8_t radio_get_status(int fd) {
	uint8_t rx[1] = { 0x00 };
	uint8_t tx[1] = { NORDIC_SPICMD_NOP };
	
	spi_transmit (fd, tx, rx, 1);
	
	return *rx;
}

radio_register_read(int fd, uint8_t reg) {
	
	int ret = 0;
	uint8_t rx[1] = { 0x00 };
	uint8_t tx[1] = { reg&0b00011111 };

	ret = spi_transmit (fd, tx, rx, ARRAY_SIZE(tx));
	
	return ret;
}

radio_register_write(int fd, uint8_t reg) {
	
}

uint8_t radio_command_send (int fd, uint8_t cmd, uint8_t *payload) {
	
} 

int main(int argc, char *argv[]) {
	int ret = 0;
	int fd;

	parse_opts(argc, argv);

	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("can't open device");

	ret = spi_init (fd);
	if (ret < 0)
		pabort("can't init SPI");
		
	ret = init_radio ();
	if (ret < 0) 
		pabort("can't init radio");
	
	//transfer(fd);

	close(fd);

	return ret;
}