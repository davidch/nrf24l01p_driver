/* nRF24L01+ commands
*/
#define NORDIC_SPICMD_R_REGISTER (0b00000000)  /* Read command and status registers. 
												  Bits 1-5 = 5 bit Register Map Address. 
												  1-5 databytes, LSByte first. */
#define NORDIC_SPICMD_W_REGISTER (0b00100000)  /* Write command and status registers. 
												  Bits 1-5 = 5 bit Register Map Address. 
												  1-5 databytes, LSByte first.
												  Executable in power down or standby modes only.*/
#define NORDIC_SPI_CMD_REGISTER_MASK (0b0001111)/* Mask (5 Least sigificant bits) to enter register number 
                                                   in W/R_REGISTER command */
#define NORDIC_SPICMD_R_RX_PAYLOAD (0b01100001) /* Read RX-payload: 1 � 32 bytes. A read operation
												  always starts at byte 0. Payload is deleted from
												  FIFO after it is read. Used in RX mode.*/
#define NORDIC_SPICMD_W_TX_PAYLOAD (0b10100000) /* Write TX-payload: 1 � 32 bytes. A write operation
												  always starts at byte 0 used in TX payload. */
#define NORDIC_SPICMD_FLUSH_TX (0b11100001)	   /* Flush TX FIFO, used in TX mode */
#define NORDIC_SPICMD_FLUSH_RX (0b11100010)	   /* Flush RX FIFO, used in RX mode
												  Should not be executed during transmission of
												  acknowledge, that is, acknowledge package will
												  not be completed.*/
#define NORDIC_SPICMD_REUSE_TX_PL (0b11100011) /* Used for a PTX device
												  Reuse last transmitted payload.
												  TX payload reuse is active until
												  W_TX_PAYLOAD or FLUSH TX is executed. TX
												  payload reuse must not be activated or deactivated
												  during package transmission.*/
#define NORDIC_SPICMD_R_RX_PL_WID (0b01100000) /* Read RX payload width for the top
												  R_RX_PAYLOAD in the RX FIFO.
												  Note: Flush RX FIFO if the read value is larger
												  than 32 bytes.
												  The bits in the FEATURE register have to be set. */
#define NORDIC_SPICMD_W_ACK_PAYLOAD (0b10101000) /* Used in RX mode.
												  Write Payload to be transmitted together with
												  ACK packet on PIPE PPP. (PPP valid in the
												  range from 000 to 101). Maximum three ACK
												  packet payloads can be pending. Payloads with
												  same PPP are handled using first in - first out
												  principle. Write payload: 1� 32 bytes. A write
												  operation always starts at byte 0. 
												  The bits in the FEATURE register have to be set. */
#define NORDIC_SPICMD_W_TX_PAYLOAD_NOACK (0b10110000) /* Used in TX mode. Disables AUTOACK on this
												  specific packet.
												  The bits in the FEATURE register have to be set.*/
#define NORDIC_SPICMD_NOP (0b11111111)   	   /* No Operation. Might be used to read the STATUS register */
 
/* Register Map
*/
#define NORDIC_REG_CONFIG (0x00)     /* Configuration Register */
#define NORDIC_REG_EN_AA (0x01)      /* Enable �Auto Acknowledgment� Function */
#define NORDIC_REG_EN_RXADDR (0x02)  /* Enabled RX Addresses */
#define NORDIC_REG_SETUP_AW (0x03)   /* Setup of Address Widths (common for all data pipes) */
#define NORDIC_REG_SETUP_RETR (0x04) /* Setup of Automatic Retransmission */
#define NORDIC_REG_RF_CH (0x05)      /* RF Channel */
#define NORDIC_REG_RF_SETUP (0x06)   /* RF Setup Register */
#define NORDIC_REG_STATUS (0x07)     /* Status Register (In parallel to the SPI command
                                        word applied on the MOSI pin, the STATUS register
                                        is shifted serially out on the MISO pin) */
#define NORDIC_REG_OBSERVE_TX (0x08) /* Transmit observe register */
#define NORDIC_REG_RPD (0x09)
#define NORDIC_REG_RX_ADDR_P0 (0x0a) /* Receive address data pipe 0. 5 Bytes maximum
                                        length. (LSByte is written first. Write the number of
                                        bytes defined by SETUP_AW) */
#define NORDIC_REG_RX_ADDR_P1 (0x0b) /* Receive address data pipe 1 */
#define NORDIC_REG_RX_ADDR_P2 (0x0c) /* Receive address data pipe 2 */
#define NORDIC_REG_RX_ADDR_P3 (0x0d) /* Receive address data pipe 3 */
#define NORDIC_REG_RX_ADDR_P4 (0x0e) /* Receive address data pipe 4 */
#define NORDIC_REG_RX_ADDR_P5 (0x0f) /* Receive address data pipe 5 */
#define NORDIC_REG_TX_ADDR (0x10)    /* Transmit address. Used for a PTX device only.
                                        (LSByte is written first)
                                        Set RX_ADDR_P0 equal to this address to handle
                                        automatic acknowledge if this is a PTX device with
                                        Enhanced ShockBurst� enabled. */
#define NORDIC_REG_RX_PW_P0 (0x11)
#define NORDIC_REG_RX_PW_P1 (0x12)
#define NORDIC_REG_RX_PW_P2 (0x13)
#define NORDIC_REG_RX_PW_P3 (0x14)
#define NORDIC_REG_RX_PW_P4 (0x15)
#define NORDIC_REG_RX_PW_P5 (0x16)
#define NORDIC_REG_FIFO_STATUS (0x17) /* FIFO Status Register */
#define NORDIC_REG_DYNPD (0x1c)       /* Enable dynamic payload length */
#define NORDIC_REG_FEATURE (0x1d)     /* Feature Register */
 
/*
    Register menmonic for each register
*/
 
/* 	NORDIC_REG_CONFIG Register Mnemonic 
	- Configuration Register */
#define NORDIC_REG_CONFIG_MASK_RX_DR  (0b01000000) /* R/W Mask interrupt caused by RX_DR
													1: Interrupt not reflected on the IRQ pin
													0: Reflect RX_DR as active low interrupt on the IRQ pin */
#define NORDIC_REG_CONFIG_MASK_TX_DS  (0b00100000) /* R/W Mask interrupt caused by TX_DS
													1: Interrupt not reflected on the IRQ pin
													0: Reflect TX_DS as active low interrupt on the IRQ pin */
#define NORDIC_REG_CONFIG_MASK_MAX_RT (0b00010000) /* R/W Mask interrupt caused by MAX_RT
													1: Interrupt not reflected on the IRQ pin
													0: Reflect MAX_RT as active low interrupt on the IRQ pin */
#define NORDIC_REG_CONFIG_EN_CRC      (0b00001000) /* R/W Enable CRC. Forced high if one of the bits in the
													EN_AA is high */
#define NORDIC_REG_CONFIG_CRCO        (0b00000100) /* R/W CRC encoding scheme
													'0' - 1 byte
													'1' � 2 bytes */
#define NORDIC_REG_CONFIG_PWR_UP      (0b00000010) /* R/W 1: POWER UP, 0:POWER DOWN */
#define NORDIC_REG_CONFIG_PRIM_RX     (0b00000001) /* R/W RX/TX control
													1: PRX, 0: PTX */
#define NORDIC_REG_CONFIG_VALUE_RESET (NORDIC_REG_CONFIG_EN_CRC)
 
/* 	NORDIC_REG_EN_AA (Enhanced ShockBurst) Register Mnemonic 
	- Enable �Auto Acknowledgment� 
	  Function Disable this functionality to be compatible with nRF2401 */
#define NORDIC_REG_EN_AA_ENAA_P5 (0b00100000) 		/* Enable auto acknowledgement data pipe 5 */
#define NORDIC_REG_EN_AA_ENAA_P4 (0b00010000) 		/* Enable auto acknowledgement data pipe 4 */
#define NORDIC_REG_EN_AA_ENAA_P3 (0b00001000) 		/* Enable auto acknowledgement data pipe 3 */
#define NORDIC_REG_EN_AA_ENAA_P2 (0b00000100) 		/* Enable auto acknowledgement data pipe 2 */
#define NORDIC_REG_EN_AA_ENAA_P1 (0b00000010) 		/* Enable auto acknowledgement data pipe 1 */
#define NORDIC_REG_EN_AA_ENAA_P0 (0b00000001) 		/* Enable auto acknowledgement data pipe 0 */
#define NORDIC_REG_EN_AA_VALUE_RESET (0b00111111)
 
/* 	NORDIC_REG_EN_RXADDR Register Mnemonic 
	- Enabled RX Addresses */
#define NORDIC_REG_EN_RXADDR_ERX_P5 (0b00100000)	/* Enable data pipe 5 */
#define NORDIC_REG_EN_RXADDR_ERX_P4 (0b00010000)	/* Enable data pipe 4 */
#define NORDIC_REG_EN_RXADDR_ERX_P3 (0b00001000)	/* Enable data pipe 3 */
#define NORDIC_REG_EN_RXADDR_ERX_P2 (0b00000100)	/* Enable data pipe 2 */
#define NORDIC_REG_EN_RXADDR_ERX_P1 (0b00000010)	/* Enable data pipe 1 */
#define NORDIC_REG_EN_RXADDR_ERX_P0 (0b00000001)	/* Enable data pipe 0 */
#define NORDIC_REG_EN_RXADDR_VALUE_RESET (0b00111111)
 
/* 	NORDIC_REG_SETUP_AW Register Mnemonic 
	- Setup of Address Widths (common for all data pipes) */
#define NORDIC_REG_SETUP_AW_AW(x) (((x)&(0b00000011)  /* RX/TX Address field width
												'00' - Illegal
												'01' - 3 bytes
												'10' - 4 bytes
												'11' � 5 bytes
												LSByte is used if address width is below 5 bytes */
#define NORDIC_REG_SETUP_AW_VALUE_RESET (NORDIC_REG_SETUP_AW_AW)
 
/* 	NORDIC_REG_SETUP_RETR Register Mnemonic
	- Setup of Automatic Retransmission */
#define NORDIC_REG_SETUP_RETR_ARD(x) (((x)&(0b00001111))<<4) /* Auto Retransmit Delay
															�0000� � Wait 250�S
															�0001� � Wait 500�S
															�0010� � Wait 750�S
															��..
															�1111� � Wait 4000�S
															(Delay defined from end of transmission to start of
															next transmission) */
#define NORDIC_REG_SETUP_RETR_ARC(x) ((x)&(0b00001111)) /* Auto Retransmit Count
														�0000� �Re-Transmit disabled
														�0001� � Up to 1 Re-Transmit on fail of AA
														��
														�1111� � Up to 15 Re-Transmit on fail of AA */
#define NORDIC_REG_SETUP_RETR_ARC_VALUE_RESET (0b00000011)

/* 	NORDIC_REG_RF_CH Register Mnemonic 
	- RF Channel */
#define NORDIC_REG_RF_CH_SET(x) ((x)&(0b01111111)) /* Sets the frequency channel nRF24L01+ operates on */
#define NORDIC_REG_RF_CH_VALUE_RESET (0b00000010)

/* 	NORDIC_REG_RF_SETUP Register Mnemonic 
	- RF Setup Register */
#define NORDIC_REG_RF_SETUP_CONT_WAVE   (0b10000000) /* Enables continuous carrier transmit when high. */
#define NORDIC_REG_RF_SETUP_RF_DR_LOW   (0b00100000) /* Set RF Data Rate to 250kbps. See RF_DR_HIGH for encoding. */
#define NORDIC_REG_RF_SETUP_PLL_LOCK    (0b00010000) /* Force PLL lock signal. Only used in test */
#define NORDIC_REG_RF_SETUP_RF_DR_HIGH  (0b00001000) /* Select between the high speed data rates. This bit
														is don�t care if RF_DR_LOW is set.
														Encoding:
														[RF_DR_LOW, RF_DR_HIGH]:
														�00� � 1Mbps
														�01� � 2Mbps
														�10� � 250kbps
														�11� � Reserved */
#define NORDIC_REG_RF_SETUP_RF_PWR      (0b00000110) /* Set RF output power in TX mode
    															'00' � -18dBm
																'01' � -12dBm
																'10' � -6dBm
																'11' � 0dBm */
#define NORDIC_REG_RF_SETUP_RF_PWR_SET(x) (((x)&(0b00000011))<<1) 
#define NORDIC_REG_RF_SETUP_RF_PWR_GET(x) (((x)>>1)&(0b00000011)) 
#define NORDIC_REG_RF_SETUP_VALUE_RESET (0b00001110)


/* 	NORDIC_REG_STATUS Register Mnemonic 
	- Status Register (In parallel to the SPI command
	  word applied on the MOSI pin, the STATUS register
	  is shifted serially out on the MISO pin) */
#define NORDIC_REG_STATUS_RX_DR  (0b01000000)	/* Data Ready RX FIFO interrupt. Asserted when
													new data arrives RX FIFOc.
													Write 1 to clear bit. */
#define NORDIC_REG_STATUS_TX_DS  (0b00100000)	/* Data Sent TX FIFO interrupt. Asserted when
													packet transmitted on TX. If AUTO_ACK is activated,
													this bit is set high only when ACK is
													received.
													Write 1 to clear bit. */
#define NORDIC_REG_STATUS_MAX_RT (0b00010000) 	/* Maximum number of TX retransmits interrupt
													Write 1 to clear bit. If MAX_RT is asserted it must
													be cleared to enable further communication. */
#define NORDIC_REG_STATUS_RX_P_NO(x) (((x)&(0b00000111))<<1) /* Data pipe number for the payload available for
																reading from RX_FIFO
																000-101: Data Pipe Number
																110: Not Used
																111: RX FIFO Empty */
#define NORDIC_REG_STATUS_TX_FULL (0b00000001) 	/* TX FIFO full flag.
													1: TX FIFO full.
													0: Available locations in TX FIFO. */
#define NORDIC_REG_STATUS_VALUE_RESET (0b00001110)

/* 	NORDIC_REG_OBSERVE_TX Register Mnemonic 
	- Transmit observe register */
#define NORDIC_REG_OBSERVE_TX_PLOS_CNT(x) (((x)&(0b00001111))<<4) /* Count lost packets. The counter is overflow protected
																	to 15, and discontinues at max until reset.
																	The counter is reset by writing to RF_CH. */
#define NORDIC_REG_OBSERVE_TX_ARC_CNT(x) (((x)&(0b00001111))) /* Count retransmitted packets. The counter is reset
																when transmission of a new packet starts. */
#define NORDIC_REG_OBSERVE_TX_VALUE_RESET (0x00)

/* 	NORDIC_REG_RPD Register Mnemonic 
	- Received Power Detector. */
#define NORDIC_REG_RPD_RPD (0b00000001) /* Received Power Detector. This register is called
											CD (Carrier Detect) in the nRF24L01. The name is
											different in nRF24L01+ due to the different input
											power level threshold for this bit. */
#define NORDIC_REG_RPD_VALUE_RESET (0x00)

/* 	NORDIC_REG_RX_ADDR_P0 Register Mnemonic 
	- Receive address data pipe 0. 5 Bytes maximum
	 length. (LSByte is written first. Write the number of
	 bytes defined by SETUP_AW) */
#define NORDIC_REG_RX_ADDR_P0_VALUE_RESET (0xe7)

/* 	NORDIC_REG_RX_ADDR_P1 Register Mnemonic 
	- Receive address data pipe 1. 5 Bytes maximum
	  length. (LSByte is written first. Write the number of
	  bytes defined by SETUP_AW) */
#define NORDIC_REG_RX_ADDR_P1_VALUE_RESET (0xe7)

/* 	NORDIC_REG_RX_ADDR_P2 Register Mnemonic 
	- Receive address data pipe 2. Only LSB. MSBytes
	  are equal to RX_ADDR_P1[39:8] */
#define NORDIC_REG_RX_ADDR_P2_VALUE_RESET (0xc3)

/* 	NORDIC_REG_RX_ADDR_P3 Register Mnemonic 
	- Receive address data pipe 3. Only LSB. MSBytes
	  are equal to RX_ADDR_P1[39:8] */
#define NORDIC_REG_RX_ADDR_P3_VALUE_RESET (0xc4)

/* 	NORDIC_REG_RX_ADDR_P4 Register Mnemonic 
	- Receive address data pipe 4. Only LSB. MSBytes
	  are equal to RX_ADDR_P1[39:8] */
#define NORDIC_REG_RX_ADDR_P4_VALUE_RESET (0xc5)

/* 	NORDIC_REG_RX_ADDR_P5 Register Mnemonic 
	- Receive address data pipe 5. Only LSB. MSBytes
	  are equal to RX_ADDR_P1[39:8] */
#define NORDIC_REG_RX_ADDR_P5_VALUE_RESET (0xc6)

/* 	NORDIC_REG_TX_ADDR Register Mnemonic 
	- Transmit address. Used for a PTX device only. (LSByte is written first)
	Set RX_ADDR_P0 equal to this address to handle automatic acknowledge 
	if this is a PTX device with Enhanced ShockBurst� enabled. */
#define NORDIC_REG_TX_ADDR_VALUE_RESET (0xe7)

/* 	NORDIC_REG_RX_PW_P0 Register Mnemonic */
#define NORDIC_REG_RX_PW_P0_SET(x) (((x)&(0b00111111))) /* Number of bytes in RX payload in data pipe 0 
															(1 to 32 bytes).
															0 Pipe not used, 1-32 = 1-32 byte */
#define NORDIC_REG_RX_PW_P0_VALUE_RESET (0x00)

/* 	NORDIC_REG_RX_PW_P1 Register Mnemonic */
#define NORDIC_REG_RX_PW_P1_SET(x) (((x)&(0b00111111)))
#define NORDIC_REG_RX_PW_P1_VALUE_RESET (0x00)

/* 	NORDIC_REG_RX_PW_P2 Register Mnemonic */
#define NORDIC_REG_RX_PW_P2_SET(x) (((x)&(0b00111111)))
#define NORDIC_REG_RX_PW_P2_VALUE_RESET (0x00)

/* 	NORDIC_REG_RX_PW_P3 Register Mnemonic */
#define NORDIC_REG_RX_PW_P3_SET(x) (((x)&(0b00111111)))
#define NORDIC_REG_RX_PW_P3_VALUE_RESET (0x00)

/* 	NORDIC_REG_RX_PW_P4 Register Mnemonic */
#define NORDIC_REG_RX_PW_P4_SET(x) (((x)&(0b00111111)))
#define NORDIC_REG_RX_PW_P4_VALUE_RESET (0x00)

/* 	NORDIC_REG_RX_PW_P5 Register Mnemonic */
#define NORDIC_REG_RX_PW_P5_SET(x) (((x)&(0b00111111)))
#define NORDIC_REG_RX_PW_P5_VALUE_RESET (0x00)

/* 	NORDIC_REG_FIFO_STATUS Register Mnemonic 
	- FIFO Status Register*/
#define NORDIC_REG_FIFO_STATUS_TX_REUSE  (0b01000000) /* Used for a PTX device
														Pulse the rfce high for at least 10�s to Reuse last
														transmitted payload. TX payload reuse is active
														until W_TX_PAYLOAD or FLUSH TX is executed.
														TX_REUSE is set by the SPI command
														REUSE_TX_PL, and is reset by the SPI commands
														W_TX_PAYLOAD or FLUSH TX */
#define NORDIC_REG_FIFO_STATUS_TX_FULL   (0b00100000) /* TX FIFO full flag. 
														1: TX FIFO full. 0: Available locations in TX FIFO. */
#define NORDIC_REG_FIFO_STATUS_TX_EMPTY  (0b00010000) /* TX FIFO empty flag.
														1: TX FIFO empty.
														0: Data in TX FIFO. */
#define NORDIC_REG_FIFO_STATUS_RX_FULL   (0b00000010) /* RX FIFO full flag.
														1: RX FIFO full.
														0: Available locations in RX FIFO. */
#define NORDIC_REG_FIFO_STATUS_RX_EMPTY  (0b00000001) /* RX FIFO empty flag.
														1: RX FIFO empty.
														0: Data in RX FIFO. */
#define NORDIC_REG_FIFO_STATUS_VALUE_RESET (0b00010001)

/* 	NORDIC_REG_DYNPD Register Mnemonic 
	- Enable dynamic payload length*/
#define NORDIC_REG_DYNPD_DPL_P5 (0b00100000) /* Enable dynamic payload length data pipe 5.
												(Requires EN_DPL and ENAA_P0) */
#define NORDIC_REG_DYNPD_DPL_P4 (0b00010000)
#define NORDIC_REG_DYNPD_DPL_P3 (0b00001000)
#define NORDIC_REG_DYNPD_DPL_P2 (0b00000100)
#define NORDIC_REG_DYNPD_DPL_P1 (0b00000010)
#define NORDIC_REG_DYNPD_DPL_P0 (0b00000001)
#define NORDIC_REG_DYNPD_VALUE_RESET (0x0)

/* 	NORDIC_REG_FEATURE Register Mnemonic 
	- Feature Register*/
#define NORDIC_REG_FEATURE_EN_DPL     (0b00000100) /* Enables Dynamic Payload Length */
#define NORDIC_REG_FEATURE_EN_ACK_PAY (0b00000010) /* Enables Payload with ACK */
#define NORDIC_REG_FEATURE_EN_DYN_ACK (0b00000001) /* Enables the W_TX_PAYLOAD_NOACK command */
#define NORDIC_REG_FEATURE_VALUE_RESET (0x00)

 
#define NORDIC_SPI_
#define NORDIC_SPI_