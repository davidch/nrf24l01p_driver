
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "radio.h"

/* global functions */
static void pabort(const char *s) {
    perror(s);
    abort();
}

static void print_usage(const char *prog) {
	printf("Usage: %s [-DsbdlHOLC3]\n", prog);
	puts("  -D --device   device to use (default /dev/spidev1.1)\n"
	     "  -s --speed    max speed (Hz)\n"
	     "  -d --delay    delay (usec)\n"
	     "  -b --bpw      bits per word \n"
	     "  -l --loop     loopback\n"
	     "  -H --cpha     clock phase\n"
	     "  -O --cpol     clock polarity\n"
	     "  -L --lsb      least significant bit first\n"
	     "  -C --cs-high  chip select active high\n"
	     "  -3 --3wire    SI/SO signals shared\n");
	exit(1);
}

static void parse_opts(int argc, char *argv[]) {
	while (1) {
		static const struct option lopts[] = {
			{ "device",  1, 0, 'D' },
			{ "speed",   1, 0, 's' },
			{ "delay",   1, 0, 'd' },
			{ "bpw",     1, 0, 'b' },
			{ "loop",    0, 0, 'l' },
			{ "cpha",    0, 0, 'H' },
			{ "cpol",    0, 0, 'O' },
			{ "lsb",     0, 0, 'L' },
			{ "cs-high", 0, 0, 'C' },
			{ "3wire",   0, 0, '3' },
			{ "no-cs",   0, 0, 'N' },
			{ "ready",   0, 0, 'R' },
			{ NULL, 0, 0, 0 },
		};
		int c;

		c = getopt_long(argc, argv, "D:s:d:b:lHOLC3NR", lopts, NULL);

		if (c == -1)
			break;

		switch (c) {
		case 'D':
			spi_config.device = optarg;
			break;
		case 's':
			spi_config.speed = atoi(optarg);
			break;
		case 'd':
			spi_config.delay = atoi(optarg);
			break;
		case 'b':
			spi_config.bits = atoi(optarg);
			break;
		case 'l':
			spi_config.mode |= SPI_LOOP;
			break;
		case 'H':
			spi_config.mode |= SPI_CPHA;
			break;
		case 'O':
			spi_config.mode |= SPI_CPOL;
			break;
		case 'L':
			spi_config.mode |= SPI_LSB_FIRST;
			break;
		case 'C':
			spi_config.mode |= SPI_CS_HIGH;
			break;
		case '3':
			spi_config.mode |= SPI_3WIRE;
			break;
		case 'N':
			spi_config.mode |= SPI_NO_CS;
			break;
		case 'R':
			spi_config.mode |= SPI_READY;
			break;
		default:
			print_usage(argv[0]);
			break;
		}
	}
}

int main(int argc, char *argv[]) {
    int ret = 0;
	int fd;

	parse_opts(argc, argv);

	fd = open(device, O_RDWR);
	if (fd < 0)
		pabort("can't open device");

	ret = spi_init (fd);
	if (ret < 0)
		pabort("can't init SPI");
		
	ret = init_radio ();
	if (ret < 0) 
		pabort("can't init radio");
	
	//transfer(fd);

	close(fd);

	return ret;
}
