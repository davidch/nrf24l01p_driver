
/* Radio data rate constants */
typedef enum radio_speed_t {
    SPEED_1MBIT = 0,
    SPEED_2MBIT = 1,
    SPEED_250KBIT = 2,
    SPEED_RESERVED = 3
} radio_speed_t;

/* radio power constants */
typedef enum radio_power_t {
    POWER_N18DB = 0,    /* -18db */
    POWER_N12DB = 1,    /* -12db */
    POWER_N6DB = 2,     /*  -6db */
    POWER_0DB = 3       /*   0db */
} radio_power_t;

/* radio handle used to access radio constants */
typedef int radio_handle_t;

/* open radio device. eg: "/dev/spidev1.1" */
radio_handle_t radio_open(char* device);
void radio_close(radio_handle_t rh);

/* Set radio speed */
int radio_speed_set(radio_handle_t rh, radio_speed_t speed);
radio_speed_t radio_speed_get(radio_handle_t rh);

/* Set radio power */
int radio_power_set(radio_handle_t rh, radio_power_t power);
radio_power_t radio_power_get(radio_handle_t rh);

/* Set radio power */
int radio_channel_set(radio_handle_t rh, int channel);
int radio_channel_get(radio_handle_t rh);

