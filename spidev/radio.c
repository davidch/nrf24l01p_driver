
#include <stdio.h>
#include <stdlib.h>

#include "nrf24l01p.h"
#include "radio.h"

static struct radio_descriptor_t {
    int file_handle;
    
    uint8_t spi_mode = 0;
	uint8_t spi_bits = 8;
	uint32_t spi_speed = 500000;
	uint16_t spi_delay;
    
    uint8_t last_status;
};
typedef struct radio_descriptor_t radio_descriptor_t *radio_handle_pt;

/* ------------------------------------
   Shared functions
   ------------------------------------*/
static int 
spi_transmit(radio_handle_pt radioh, uint8_t* buffer_tx, uint8_t* buffer_rx, uint8_t len) {
    
    int ret = 0;
    
    /* prepare the SPI IOCTL transfer structure */
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)buffer_tx,
		.rx_buf = (unsigned long)buffer_rx,
		.len = len,
		.delay_usecs = radioh->spi_delay,
		.speed_hz = radioh->spi_speed,
		.bits_per_word = radioh->spi_bits,
	};

	ret = ioctl(radioh->file_handle, SPI_IOC_MESSAGE(1), &tr);
    
	return ret;
}
static int 
radio_register_read (radio_handle_pt radioh, uint8_t reg, uint8_t *data, int len) {
    uint8_t rx[6];
    uint8_t tx[6] = {0xff,0xff,0xff,0xff,0xff,0xff};
    int ret = 0;
    
    /* max len for R_REGISTER command is 5 */
    if (len > 5) 
        len = 5;
    
    /* first byte is R_REGISTER command */
    tx[0] = (NORDIC_SPICMD_R_REGISTER | (reg&NORDIC_SPI_CMD_REGISTER_MASK));
    
    ret = spi_transmit(radioh, tx, rx, len);
    
    if (ret >= 0) {
        /* first byte always contains radio STATUS register */
        radioh->last_status = rx[0];
        
        /* copy the remaining data received into the output buffer */
        while (len--)
            *data++ = *((rx+1)++);
    }
    return ret;
}
static int 
radio_register_write (radio_handle_pt radioh, uint8_t reg, uint8_t *data, int len) {
    uint8_t rx[6];
    uint8_t tx[6] = {0xff,0xff,0xff,0xff,0xff,0xff};
    int ret = 0;
    int j = 0;
    
    /* first byte is W_REGISTER command */
    tx[0] = (NORDIC_SPICMD_W_REGISTER | (reg&NORDIC_SPI_CMD_REGISTER_MASK));
    
    /* max len for W_REGISTER command is 5 */
    if (len > 5) 
        len = 5;
    
    /* copy the remaining bytes to transmit */
    while (j++ < len)
        tx[1+j] = data[j];
    
    ret = spi_transmit(radioh, tx, rx, len);
    
    if (ret >= 0) {
        /* first byte always contains radio STATUS register */
        radioh->last_status = rx[0];
    }
    
    return ret;
}

/* open radio device. eg: "/dev/spidev1.1" */
radio_handle_t radio_open(char* device) {
    int fd;
    
    /* open radio device passed as argument */
    fd = open(device, O_RDWR);
    if (fd >= 0)
    {
        /* allocate new radio decriptor structure if device was opened successfully */
        radio_handle_pt rh = (radio_handle_pt) calloc (1, sizeof(radio_descriptor_t));
        
        if (NULL == rh)
            (radio_handle_t)(-1);
            
        /* initialize descriptor values */
        rh->file_handle = fd;

        spi_mode = 0;
        spi_bits = 8;
    	spi_speed = 500000;
    	spi_delay = 5000;      /* 5ms ? */
    
        return (radio_handle_t)(rh);
    }
    return (radio_handle_t)(-1);
}
/* close radio device */
void radio_close(radio_handle_t rh) {
    radio_handle_pt radio = (radio_handle_pt) rh;
    
    /* close associated file handle */
    close(radio->file_handle);
    
    /* free-up descriptor structure from memory */
    free ((void*)rh);
}

/* Set radio speed */
int radio_speed_set(radio_handle_t rh, radio_speed_t speed) {
    radio_handle_pt radio = (radio_handle_pt) rh;
    uint8_t register_value;
    int ret = 0;
    
    /* first read the SETUP register */
    ret = radio_register_read (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
    
    if (ret < 0)
        return ret;
        
    /* clear the speed controlling bits */
    register_value &= ~(NORDIC_REG_RF_SETUP_RF_DR_LOW|NORDIC_REG_RF_SETUP_RF_DR_HIGH);
    /* set the HIGH bit */
    register_value |= (((unsigned int)speed&1)?(NORDIC_REG_RF_SETUP_RF_DR_HIGH):(0));
    register_value |= (((unsigned int)speed&2)?(NORDIC_REG_RF_SETUP_RF_DR_LOW):(0));
    
    /* write the SETUP register with new speed value back to radio */
    ret = radio_register_write (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
                            
    return ret;
}
radio_speed_t radio_speed_get(radio_handle_t rh) {
    radio_handle_pt radio = (radio_handle_pt) rh;
    uint8_t register_value;
    int ret = 0
    radio_speed_t speed;
    
    /* read the radio SETUP register */
    ret = radio_register_read (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
    
    if (ret < 0)
        return (radio_speed_t)ret;
        
    if (register_value&NORDIC_REG_RF_SETUP_RF_DR_LOW) {
        if (register_value&NORDIC_REG_RF_SETUP_RF_DR_HIGH)
            return radio_speed_t.SPEED_RESERVED;
        else
            return radio_speed_t.SPEED_250KBIT;
    } else 
        if (register_value&NORDIC_REG_RF_SETUP_RF_DR_HIGH)
            return radio_speed_t.SPEED_2MBIT;
    
    return radio_speed_t.SPEED_1MBIT;
}

/* Set radio power */
int radio_power_set(radio_handle_t rh, radio_power_t power) {
    radio_handle_pt radio = (radio_handle_pt) rh;
    uint8_t register_value;
    int ret = 0;
    
    /* first read the SETUP register */
    ret = radio_register_read (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
    
    if (ret < 0)
        return ret;
        
    /* clear the power controlling bits */
    register_value &= ~(NORDIC_REG_RF_SETUP_RF_PWR);
    /* set the new power */
    register_value |= NORDIC_REG_RF_SETUP_RF_PWR_SET((unsigned int)power);
    
    /* write the SETUP register with new speed value back to radio */
    ret = radio_register_write (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
                            
    return ret;
}
radio_power_t radio_power_get(radio_handle_t rh)
{
    radio_handle_pt radio = (radio_handle_pt) rh;
    uint8_t register_value;
    int ret = 0;
    
    /* first read the SETUP register */
    ret = radio_register_read (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
    
    if (ret < 0)
        return ret;
        
    /* clear the power controlling bits */
    register_value &= ~(NORDIC_REG_RF_SETUP_RF_PWR);
    /* set the new power */
    register_value |= NORDIC_REG_RF_SETUP_RF_PWR_SET((unsigned int)power);
    
    /* write the SETUP register with new speed value back to radio */
    ret = radio_register_write (&radio, NORDIC_REG_RF_SETUP, &register_value, 1);
                            
    return ret;
}

/* Set radio power */
int radio_channel_set(radio_handle_t rh, int channel);
int radio_channel_get(radio_handle_t rh);