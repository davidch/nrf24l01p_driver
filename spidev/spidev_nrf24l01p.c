/*
 * SPI testing utility (using spidev driver)
 *
 * Copyright (c) 2007  MontaVista Software, Inc.
 * Copyright (c) 2007  Anton Vorontsov <avorontsov@ru.mvista.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License.
 *
 * Cross-compile with cross-gcc -I/path/to/cross-kernel/include
 */

#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

/* macro defines 
*/
#define _DEBUG
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define INITIAL_RADIO_BUFFER_LEN (100)

/* structure defines 
*/
static struct spi_config_t spi_config {
	char *device = "/dev/spidev1.1";
	uint8_t mode = 0;
	uint8_t bits = 8;
	uint32_t speed = 500000;
	uint16_t delay;
} = {
	/* default values */
	/* char *device =   */ "/dev/spidev1.1", 
	/* uint8_t mode =   */ 0,
	/* uint8_t bits =   */ 8,
	/* uint32_t speed = */ 500000,
	/* uint16_t delay = */ 1
};

static struct spi_descriptor_t radio_descriptor {
    int file_handle;
    struct spi_config_t *config;
    void()* lock;
    void()* unlock;
    uint8_t *buffer_rx;
    uint8_t *buffer_tx;
    int buffer_len;
} = {0,NULL,NULL,NULL,NULL,NULL,0};
typedef struct spi_descriptor_t spi_descriptor_t;


int spi_transmit(spi_descriptor_t *spid, uint8_t* buffer_tx, uint8_t* buffer_rx, uint8_t len) {
	
    int ret = 0;
    
    /* prepare the SPI IOCTL transfer structure */
	struct spi_ioc_transfer tr = {
		.tx_buf = (unsigned long)buffer_tx,
		.rx_buf = (unsigned long)buffer_rx,
		.len = len,
		.delay_usecs = spid->config.delay,
		.speed_hz = spid->config.speed,
		.bits_per_word = spid->config.bits,
	};

	ret = ioctl(spid->file_handle, SPI_IOC_MESSAGE(1), &tr);
    
	return ret;
}

int spi_setup(int fd) {
	int ret = 0;

	/*
	 * spi mode
	 */
	ret = ioctl(fd, SPI_IOC_WR_MODE, &spi_config.mode);
	if (ret == -1)
		pabort("can't set spi mode");

	ret = ioctl(fd, SPI_IOC_RD_MODE, &spi_config.mode);
	if (ret == -1)
		pabort("can't get spi mode");

	/*
	 * bits per word
	 */
	ret = ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &spi_config.bits);
	if (ret == -1)
		pabort("can't set bits per word");

	ret = ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &spi_config.bits);
	if (ret == -1)
		pabort("can't get bits per word");

	/*
	 * max speed hz
	 */
	ret = ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &spi_config.speed);
	if (ret == -1)
		pabort("can't set max speed hz");

	ret = ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &spi_config.speed);
	if (ret == -1)
		pabort("can't get max speed hz");

	printf("spi mode: %d\n", spi_config.mode);
	printf("bits per word: %d\n", spi_config.bits);
	printf("max speed: %d Hz (%d KHz)\n", spi_config.speed, spi_config.speed/1000);

	return ret;
}

/* Read radio STATUS register
   An NOP command is sent over SPI to radio which 
   will send back 8 bits of STATUS register */
uint8_t radio_get_status(spi_descriptor_t *spid) {
	uint8_t rx[1] = { 0x00, 0x00 };
	uint8_t tx[1] = { NORDIC_SPICMD_NOP, 0xff };
	
	spi_transmit (spid->file_handle, tx, rx, 2);
	
	return *(rx+1);
}

int radio_register_read(spi_descriptor_t *spid, uint8_t reg, uint8_t* buffer_rx, uint8_t len) {
	
	int ret = 0;
    
    /* buffer management code */
    if (len => spid->buffer_len) {
        int new_len = (len + len / 5) * 2 + 1; /* add 20% to the desired space to save frequent realloc */  
        uint8_t = buffer_new = NULL;
        
        buffer_new = realloc (spid->buffer_tx, buffer_tx, new_len * sizeof (*spid->bufer_tx));
        if (NULL == buffer_new)
            pabort ("Memory problem. Realloc failed");
        
        spid->buffer_len = new_len / 2;
        spid->buffer_rx = buffer_new;
        spid->buffer_tx = spid->buffer_rx + spid->buffer_len;
    }
    
    /* set the transmit buffer header */
	spid->buffer_tx[1] = { (NORDIC_SPICMD_R_REGISTER | (reg&NORDIC_SPI_CMD_REGISTER_MASK)) };
    /* clear the remainder of the buffer with xFF to allow receive for the entire message length requested */
    memset (spid->buffer_tx+1, 0xff, len);

	ret = spi_transmit (spid, spid->buffer_tx, spid->buffer_rx, len);
	
	return ret;
}
int radio_register_write(spi_descriptor_t *spid, uint8_t reg, uint8_t* buffer_tx, uint8_t len) {
    
    int ret = 0;
    
    /* buffer management code */
    if (len => spid->buffer_len) {
        int new_len = (len + len / 5) * 2 + 1; /* add 20% to the desired space to save frequent realloc */  
        uint8_t = buffer_new = NULL;
        
        buffer_new = realloc (spid->buffer_tx, buffer_tx, new_len * sizeof (*spid->bufer_tx));
        if (NULL == buffer_new)
            pabort ("Memory problem. Realloc failed");
        
        spid->buffer_len = new_len / 2;
        spid->buffer_rx = buffer_new;
        spid->buffer_tx = spid->buffer_rx + spid->buffer_len;
    }
    
    /* set the transmit buffer header */
    spid->buffer_tx[1] = { (NORDIC_SPICMD_W_REGISTER | (reg&NORDIC_SPI_CMD_REGISTER_MASK)) };
    /* clear the remainder of the buffer with xFF to allow receive for the entire message length requested */
    memcpy (spid->buffer_tx+1, buffer_tx, len);

	ret = spi_transmit (spid, spid->buffer_tx, spid->buffer_rx, len+1);
	
	return ret;
}

uint8_t radio_command_send (int fd, uint8_t cmd, uint8_t *payload) {
	
} 

int radio_init(int fd) {
    int ret = 0;
    uint8_t radio_status = 0;
    uint8_t register_value = 0;
	
    /* set up descriptor */
    radio_descriptor.file_handle = fd;
    radio_descriptor.buffer_len = INITIAL_RADIO_BUFFER_LEN;
    radio_descriptor.buffer_rx = (uint8_t*) malloc (INITIAL_RADIO_BUFFER_LEN*2);
    radio_descriptor.buffer_tx = radio_descriptor.buffer_rx + radio_descriptor.buffer_len;
    radio_descriptor.lock = radio_descriptor.unlock = NULL;
    radio_descriptor.config = &spi_config;
    
    /*  When the VDD reaches 1.9V or higher nRF24L01+ enters 
        the Power on reset state where it remains in
        reset until entering the Power Down mode. */
    //TODO: wait 100ms
    radio_status = radio_get_status(&radio_descriptor);
    
    /* set maximum address width (5 bytes) */
    radio_register_write (  &radio_descriptor, 
                            NORDIC_REG_SETUP_AW, 
                            register_value = NORDIC_REG_SETUP_AW_AW(0b00000011)); 
                            
    /* set default channel */
    radio_register_write (  &radio_descriptor, 
                            NORDIC_REG_RF_CH, 
                            register_value = NORDIC_REG_RF_CH_SET(50)); 

    
    /* set speed */
    radio_register_write (  &radio_descriptor, 
                            NORDIC_REG_SETUP_AW, 
                            register_value = NORDIC_REG_SETUP_AW_AW(0b00000011)); 

    /* set power */
    radio_register_write (  &radio_descriptor, 
                            NORDIC_REG_SETUP_AW, 
                            register_value = NORDIC_REG_SETUP_AW_AW(0b00000011)); 
    
	return ret;
}


